package com.lucifer.utils;

import com.lucifer.enums.DateType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能描述:
 * <p>
 * Created by Mr.wang on 2017/6/9 11:15.
 */
public class DateFormatter {
    /**
     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
     */
    public static String getNowDate(DateType formatType) {
        Date nowTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(formatType.toString());
        String dateStr = formatter.format(nowTime);
        return dateStr;
    }

    /**
     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
     */
    public static String getNowDate() {
        Date nowTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(DateType.YYYYMMDD_HHMMSS.toString());
        String dateStr = formatter.format(nowTime);
        return dateStr;
    }

    /**
     * 将时间戳转换为时间
     *
     * @param s
     * @return
     */
    public static String stampToDate(String s, DateType formatType) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatType.toString());
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 将时间戳转换为时间
     *
     * @param s
     * @return
     */
    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateType.YYYYMMDD_HHMMSS.toString());
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 将时间转换为时间戳
     *
     * @param s
     * @return
     */
    public static String dateToStamp(String s) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateType.YYYYMMDD_HHMMSS.toString());
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        String res = String.valueOf(ts);
        return res;
    }

    /**
     * 将时间转换为时间戳
     *
     * @param s
     * @return
     */
    public static String dateToStamp(String s, DateType formatType) throws ParseException {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatType.toString());
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        res = String.valueOf(ts);
        return res;
    }
}
