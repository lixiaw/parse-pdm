package com.lucifer.entity;

import java.util.List;

/**
 * Created by Mr.Wang on 2017/6/10.
 */
public class PdmModel {

    List<PdmFile> pdmFiles;
    List<PdmPackage> pdmPackages;

    public List<PdmFile> getPdmFiles() {
        return pdmFiles;
    }

    public void setPdmFiles(List<PdmFile> pdmFiles) {
        this.pdmFiles = pdmFiles;
    }

    public List<PdmPackage> getPdmPackages() {
        return pdmPackages;
    }

    public void setPdmPackages(List<PdmPackage> pdmPackages) {
        this.pdmPackages = pdmPackages;
    }

    @Override
    public String toString() {
        StringBuffer pdmModelStr = new StringBuffer();
        pdmModelStr.append("PdmModel{\n");

        for (int i = 0; i < pdmFiles.size(); i++) {
            pdmModelStr.append("    pdmFiles[" + i + "]:" + pdmFiles.get(i).toString() + "\n");
        }
        for (int i = 0; i < pdmPackages.size(); i++) {
            pdmModelStr.append("    pdmPackages[" + i + "]:" + pdmPackages.get(i).toString() + "\n");
        }
        pdmModelStr.append("}");
        return pdmModelStr.toString();
    }
}
