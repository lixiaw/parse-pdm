package com.lucifer.entity;

/**
 * 功能描述:
 * <p> PDM   列
 * Created by Mr.wang on 2017/6/7 00:09.
 */
public class PdmColumn {

    String id;
    String name;
    String code;
//    String creationDate;
//    String creator;
//    String modificationDate;
//    String modifier;
    String comment;
    String dataType;
    String isPk;
    String length;
    String mandatory;//暂时推测为必填

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getIsPk() {
        return isPk;
    }

    public void setIsPk(String isPk) {
        this.isPk = isPk;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }


    @Override
    public String toString() {
        return "PdmColumn{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", comment='" + comment + '\'' +
                ", isPk='" + isPk + '\'' +
                ", dataType='" + dataType + '\'' +
                ", length='" + length + '\'' +
                ", mandatory='" + mandatory + '\'' +
                '}';
    }
}
