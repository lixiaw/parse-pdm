package com.lucifer.entity;

import java.util.List;

/**
 * 功能描述:
 * <p> 有键的列
 * Created by Mr.wang on 2017/6/7 00:14.
 */
public class PdmKeyColumn {
    String id;
    List<String> ref;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getRef() {
        return ref;
    }

    public void setRef(List<String> ref) {
        this.ref = ref;
    }

    @Override
    public String toString() {
        return "PdmKeyColumn{" +
                "id='" + id + '\'' +
                ", ref='" + ref + '\'' +
                '}';
    }
}
