package com.lucifer.entity;

import java.util.List;

/**
 * 功能描述:
 * <p> PDM 键
 * Created by Mr.wang on 2017/6/7 00:11.
 */
public class PdmKey {
    String id;
    String name;
    String code;
    String creationDate;
    String creator;
    String modificationDate;
    String modifier;
    PdmKeyColumn pdmKeyColumn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public PdmKeyColumn getPdmKeyColumn() {
        return pdmKeyColumn;
    }

    public void setPdmKeyColumn(PdmKeyColumn pdmKeyColumn) {
        this.pdmKeyColumn = pdmKeyColumn;
    }

    @Override
    public String toString() {
        return "PdmKey{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", creator='" + creator + '\'' +
                ", modificationDate='" + modificationDate + '\'' +
                ", modifier='" + modifier + '\'' +
                ", pdmKeyColumn=" + pdmKeyColumn +
                '}';
    }
}
