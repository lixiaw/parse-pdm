package com.lucifer.entity;

import java.util.List;

/**
 * 功能描述:
 * <p>
 * Created by Mr.wang on 2017/6/9 11:11.
 */
public class PdmCommon {
    String id;
    String code;
    String name;
    String creationDate;
    String creator;
    String modificationDate;
    String modifier;
    String comment;
    List<PdmColumn> pdmColumns;
    List<PdmKey> pdmKeys;
    List<PdmPKey> primaryKey;


    @Override
    public String toString() {
        return "PdmCommon{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", creator='" + creator + '\'' +
                ", modificationDate='" + modificationDate + '\'' +
                ", modifier='" + modifier + '\'' +
                ", comment='" + comment + '\'' +
                ", pdmColumns=" + pdmColumns +
                ", pdmKeys=" + pdmKeys +
                ", primaryKey=" + primaryKey +
                '}';
    }
}
