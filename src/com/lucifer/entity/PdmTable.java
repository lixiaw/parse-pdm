package com.lucifer.entity;

import java.util.List;

/**
 * 功能描述:
 * <p> PDM  表
 * Created by Mr.wang on 2017/6/7 00:07.
 */
public class PdmTable {

    String id;
    String code;
    String name;
    String creationDate;
    String creator;
    String modificationDate;
    String modifier;
    String comment;
    List<PdmColumn> pdmColumns;
    List<PdmKey> pdmKeys;
    List<PdmPKey> primaryKey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<PdmColumn> getPdmColumns() {
        return pdmColumns;
    }

    public void setPdmColumns(List<PdmColumn> pdmColumns) {
        this.pdmColumns = pdmColumns;
    }

    public List<PdmKey> getPdmKeys() {
        return pdmKeys;
    }

    public void setPdmKeys(List<PdmKey> pdmKeys) {
        this.pdmKeys = pdmKeys;
    }

    public List<PdmPKey> getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(List<PdmPKey> primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public String toString() {
        return "PdmTable{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", creator='" + creator + '\'' +
                ", modificationDate='" + modificationDate + '\'' +
                ", modifier='" + modifier + '\'' +
                ", comment='" + comment + '\'' +
                ", pdmColumns=" + pdmColumns +
                ", pdmKeys=" + pdmKeys +
                ", primaryKey=" + primaryKey +
                '}';
    }
}
