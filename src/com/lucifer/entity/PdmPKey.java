package com.lucifer.entity;

/**
 * 功能描述:
 * <p> 主键
 * Created by Mr.wang on 2017/6/7 00:15.
 */
public class PdmPKey {
    String ref;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    @Override
    public String toString() {
        return "PdmPKey{" +
                "ref='" + ref + '\'' +
                '}';
    }
}
