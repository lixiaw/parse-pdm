package com.lucifer.entity;

import java.util.List;

/**
 * Created by Mr.Wang on 2017/6/9.
 */
public class PdmPackage {
    String id;
    String code;
    String name;
    String creationDate;
    String creator;
    String modificationDate;
    String modifier;
    List<PdmTable> pdmTables;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public List<PdmTable> getPdmTables() {
        return pdmTables;
    }

    public void setPdmTables(List<PdmTable> pdmTables) {
        this.pdmTables = pdmTables;
    }

    @Override
    public String toString() {
        return "PdmPackage{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", creator='" + creator + '\'' +
                ", modificationDate='" + modificationDate + '\'' +
                ", modifier='" + modifier + '\'' +
                ", pdmTables=" + pdmTables +
                '}';
    }
}
