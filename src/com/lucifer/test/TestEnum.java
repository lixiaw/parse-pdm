package com.lucifer.test;


import com.lucifer.enums.ParsePdmType;

import java.util.EnumMap;
import java.util.EnumSet;

/**
 * 功能描述:
 * <p>
 * Created by Mr.wang on 2017/6/6 17:22.
 */
public class TestEnum {

    public static void main(String[] args) {
        System.out.println(ParsePdmType.PDM_MODEL.toString());
        System.out.println("演示枚举类型的遍历");
        testTraversalEnum();
        System.out.println("演示EnumMap的使用");
        testEnumMap();
        System.out.println("演示EnumSet的使用");
        testEnumSet();
    }

    /**
     *   演示枚举类型的遍历
     */
    private static void testTraversalEnum() {
        ParsePdmType[] allParsePdmType = ParsePdmType.values();
        for (ParsePdmType aParsePdmType : allParsePdmType) {
            System. out .println( " 当前 name ： " + aParsePdmType.name());
            System. out .println( " 当前 ordinal ： " + aParsePdmType.ordinal());
            System. out .println( " 当前 obj： " + aParsePdmType);
        }
    }

    /**
     *   演示 EnumMap 的使用， EnumMap 跟 HashMap 的使用差不多，只不过 key 要是枚举类型
     */
    private static void testEnumMap() {
        // 1. 演示定义 EnumMap 对象， EnumMap 对象的构造函数需要参数传入 , 默认是 key 的类的类型
        EnumMap<ParsePdmType, String> currEnumMap = new EnumMap<ParsePdmType, String>(
                ParsePdmType. class );
        currEnumMap.put(ParsePdmType.PDM_MODEL , "PDM_MODEL" );
        currEnumMap.put(ParsePdmType.PDM_PACKAGE , "PDM_PACKAGE" );
        currEnumMap.put(ParsePdmType.PDM_TABLE , "PDM_TABLE" );
        currEnumMap.put(ParsePdmType.PDM_COLUMN , "PDM_COLUMN" );

        // 2. 遍历对象
        for (ParsePdmType aParsePdmType : ParsePdmType.values ()) {
            System. out .println( "[key=" + aParsePdmType.name() + ",value="
                    + currEnumMap.get(aParsePdmType) + "]" );
        }
    }

    /**
     *   演示 EnumSet 如何使用， EnumSet 是一个抽象类，获取一个类型的枚举类型内容 <BR/>
     *   可以使用 allOf 方法
     */
    private static void testEnumSet() {
        EnumSet<ParsePdmType> currEnumSet = EnumSet.allOf (ParsePdmType.class );
        for (ParsePdmType aParsePdmTypeSetElement : currEnumSet) {
            System. out .println( " 当前 EnumSet 中数据为： " + aParsePdmTypeSetElement);
        }

    }
}
