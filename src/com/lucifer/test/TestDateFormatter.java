package com.lucifer.test;

import com.lucifer.enums.DateType;
import com.lucifer.utils.DateFormatter;

/**
 * Created by Mr.Wang on 2017/6/10.
 */
public class TestDateFormatter {

    public static void main(String[] args) {
        String nowTime = DateFormatter.getNowDate(DateType.YYYYMMDD_HHMMSS);
        System.out.println("nowTime:" + nowTime.toString());
        String s = DateFormatter.stampToDate("1467869350", DateType.YYYYMMDD_HHMMSS);
        System.out.println("时间戳转换为时间:" + s);
    }
}
