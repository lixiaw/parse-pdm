package com.lucifer.enums;

/**
 * 功能描述:
 * <p>
 * Created by Mr.wang on 2017/6/9 12:13.
 */
public enum DateType {
    YYYYMMDD_HHMMSS("yyyy-MM-dd HH:mm:ss"),
    YYYYMMDD("yyyyMMdd"),
    YYYY_MM_DD("yyyy-MM-dd");

    private String sCode;

    DateType(String sCode) {
        this.sCode = sCode;
    }

    @Override
    public String toString() {
        return String.valueOf(this.sCode);
    }
}
