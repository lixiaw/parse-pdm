package com.lucifer.enums;

/**
 * 功能描述:
 * <p>
 * Created by Mr.wang on 2017/6/6 17:07.
 */
public enum ParsePdmType {
    PDM_FROM_FILE("c:Files/o:FileObject"),
    PDM_FROM_PACKAGE("c:Packages/o:Package"),
    PDM_MODEL("/Model/o:RootObject/c:Children/o:Model"),
    PDM_PACKAGE("/Model/o:RootObject/c:Children/o:Model/c:Packages/o:Package"),
    PDM_TABLE("/Model/o:RootObject/c:Children/o:Model/c:Packages/o:Package/c:Tables/o:PdmTable"),
    PDM_FILE("/Model/o:RootObject/c:Children/o:Model/c:Files/o:FileObject"),
    PDM_COLUMN("/Model/o:RootObject/c:Children/o:Model/c:Packages/o:Package/c:Tables/o:PdmTable/c:Columns/o:PdmColumn");

    private String sCode;

    ParsePdmType(String sCode) {
        this.sCode = sCode;
    }

    @Override
    public String toString() {
        return String.valueOf(this.sCode);
    }
}
